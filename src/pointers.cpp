#include <iostream>

using namespace std;


int main()
{
	int x = 25, y = 2 * x;    
	auto a = &x, b = a;
	auto c = *a, d = 2 * *b;
	
	//integer x = 25, interger y = 50
	//pointer a = address of x, pointer b = address of x
	//integer c = 25 , integer d = 50

	cout<<"value of x = "<<x<<", address of x = "<<&x<<endl;
	       
	cout<<"value of y = "<<y<<", address of y = "<<&y<<endl;
	        
	cout<<"value of a = "<<a<<", address of a = "<<&a<<endl;
	       
	cout<<"value of b = "<<b<<", address of b = "<<&b<<endl;
	       
	cout<<"value of c = "<<c<<", address of c = "<<&c<<endl;
	        
	cout<<"value of d = "<<d<<", address of d = "<<&d<<endl;

}
